-- Sacar el actor principal de cada pelicula
USE videoclub;
SELECT p.Titol, a.Nom FROM PELICULA AS p
JOIN ACTOR AS a
ON p.CodiActor = a.CodiActor;