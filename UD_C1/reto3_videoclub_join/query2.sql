-- Sacar todas las facturas de la persona Maria
USE videoclub;
SELECT c.* ,f.CodiFactura, f.Data, f.Import 
FROM FACTURA AS f
JOIN
CLIENT as c
ON f.DNI = c.DNI
WHERE c.Nom LIKE "Maria%";