-- Sacar Nombre de la película y el nombre del género
USE videoclub;
SELECT p.Titol, g.Descripcio
FROM
PELICULA AS p
JOIN
GENERE AS g
ON p.CodiGenere = g.CodiGenere;