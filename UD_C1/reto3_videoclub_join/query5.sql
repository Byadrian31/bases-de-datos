-- Lista ID y nombres de las películas junto a los ID y nombres de sus segundas partes.
USE videoclub;
SELECT pg.CodiPeli AS "Primera parte",
pg.Titol,
sg.CodiPeli AS "Segunda parte",
sg.Titol
FROM PELICULA AS pg
JOIN PELICULA AS sg
ON pg.CodiPeli = sg.SegonaPart;