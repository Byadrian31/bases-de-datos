# Reto 2: Consultas básicas con JOIN

Adrián López Pascual

En este reto trabajamos con la base de datos `videoclub` , que nos viene dada en el fichero `videoclub.sql`. A continuación comenzaremos a prácticar las consultas usando los join(usando alias en todas las tablas para identificar cada tabla).

* [Enlace al repo](https://gitlab.com/Byadrian31/bases-de-datos)
* [Enlace al ejercicio](https://gitlab.com/Byadrian31/bases-de-datos/-/tree/main/UD_C1/reto3_videoclub_join?ref_type=heads)


## Query 1
Para sacar el nombre de la película y el nombre de su género, realizamos un join sobre las tablas `PELICULA` y `GENERE`(poniendole un ALIAS a cada una), y usamos la sentencia `ON` para indicar que valores se usarán para "unificar las tablas", en este caso son `p.CodiGenere` y  `g.CodiGenere`:

```sql
-- Sacar Nombre de la película y el nombre del género
USE videoclub;
SELECT p.Titol, g.Descripcio
FROM
PELICULA AS p
JOIN
GENERE AS g
ON p.CodiGenere = g.CodiGenere;
```


## Query 2
Para sacar todas las facturas, seleccionaremos todas las columnas de la tabla `CLIENT`(`*`) y -`CodiFactura`, `Data`, `Import`- de la tabla `Factura`, para poder realizar esto usaremos un `JOIN` unficiando las tablas con `ON` sobre los valores `DNI` de ambas tablas y para finalizar la consulta creamos un filtro con `LIKE` para sacar el nombre `Maria`:

```sql
-- Sacar todas las facturas de la persona Maria
USE videoclub;
SELECT c.* ,f.CodiFactura, f.Data, f.Import 
FROM FACTURA AS f
JOIN
CLIENT as c
ON f.DNI = c.DNI
WHERE c.Nom LIKE "Maria%";
```


## Query 3
Para sacar el actor principal de cada película, seleccionaremos `Titol` de la tabla `PELICULA` y `Nom` de la tabla `Actor`, para poder realizar esto usaremos un `JOIN` unficiando las tablas con `ON` sobre los valores `CodiActor`:

```sql
-- Sacar el actor principal de cada pelicula
USE videoclub;
SELECT p.Titol, a.Nom FROM PELICULA AS p
JOIN ACTOR AS a
ON p.CodiActor = a.CodiActor;
```

## Query 4
Modificando el código anterior, añadimos un segundo `JOIN` para comparar la tabla `INTERPRETADA`(realizando una consulta sobre 3 tablas) con la tabla `ACTOR`, además de usar `ON` para comparar la columna `CodiPeli` de las tablas `PELICULA` y `INTERPRETADA` además de comparar a la vez la columna `CodiActor` de las tablas `ACTOR` y `INTERPRETADA`: 

```sql
-- Sacar todos los actores de cada pelicula
USE videoclub;
SELECT p.Titol, a.Nom FROM
PELICULA AS p
JOIN INTERPRETADA AS i
JOIN ACTOR AS a
ON p.CodiPeli = i.CodiPeli
AND a.CodiActor = i.CodiActor;
```

## Query 5
En esta consulta hay que realizar un `JOIN` sobre la misma tabla para poder comparar las columnas (`pg = Primera parte`)`CodiPeli` y (`sg = Segunda parte`)`SegonaPart`, pudiendo así obtener los valores requeridos:

```sql
-- Lista ID y nombres de las películas junto a los ID y nombres de sus segundas partes.
USE videoclub;
SELECT pg.CodiPeli AS "Primera parte",
pg.Titol,
sg.CodiPeli AS "Segunda parte",
sg.Titol
FROM PELICULA AS pg
JOIN PELICULA AS sg
ON pg.CodiPeli = sg.SegonaPart;
```