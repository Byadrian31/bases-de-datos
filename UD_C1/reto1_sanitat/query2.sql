-- Muestre los hospitales existentes (número, nombre y teléfono) que tengan una letra A en la segunda posición del nombre.
USE sanitat;
SELECT HOSPITAL_COD AS 'Cógio hospital', NOM , TELEFON FROM HOSPITAL
	WHERE NOM LIKE '_a%';