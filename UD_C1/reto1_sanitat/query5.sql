-- Muestre a los enfermos nacidos en 1960.
USE sanitat;
SELECT * FROM MALALT
	WHERE YEAR(DATA_NAIX) = 1960;