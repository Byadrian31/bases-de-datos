-- Muestre los trabajadores (código hospital, código sala, número empleado y apellido) existentes.
USE sanitat;
SELECT HOSPITAL_COD AS 'Código Hospital', SALA_COD AS 'Código Sala', EMPLEAT_NO AS 'Número empleado', COGNOM FROM PLANTILLA;