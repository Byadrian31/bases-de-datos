-- Muestre a los enfermos nacidos a partir del año 1960.
USE sanitat;
SELECT * FROM MALALT
	WHERE YEAR(DATA_NAIX) >= 1960;