# Reto 1: Consultas básicas

Adrián López Pascual

En este reto trabajamos con la base de datos `sanitat`, que nos viene dada en el fichero `sanitat.sql`. A continuación realizamos una serie de consultas para extraer la información pedida en cada uno de los enunciados.

* [Enlace al repo](https://gitlab.com/Byadrian31/bases-de-datos)
* [Enlace al ejercicio](https://gitlab.com/Byadrian31/bases-de-datos/-/tree/main/UD_C1/reto1_sanitat?ref_type=heads)


## Query 1
Para seleccionar el número, nombre y teléfono de todos los hospitales existentes, seleccionaremos estos tres atributos, que se corresponden con las columnas `HOSPITAL_COD`, `NOM`, y `TELEFON`, respectivamente, de la tabla `HOSPITAL`. Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
-- Muestre los hospitales existentes (número, nombre y teléfono).
USE sanitat;
SELECT HOSPITAL_COD AS 'Cógio hospital', NOM , TELEFON FROM HOSPITAL;
```


## Query 2
Selecciono los tres atributos correspondientes con el enunciado(`HOSPITAL_COD`, `NOM`, y `TELEFON`) sobre la tabla `HOSPITAL`. Posteriormente filtrarlo con `LIKE` para sacar todos los valores que en su nombre la segunda letra sea una A:

```sql
-- Muestre los hospitales existentes (número, nombre y teléfono) que tengan una letra A en la segunda posición del nombre.
USE sanitat;
SELECT HOSPITAL_COD AS 'Cógio hospital', NOM , TELEFON FROM HOSPITAL
	WHERE NOM LIKE '_a%';
```


## Query 3
En este código selecciono los atributos: `HOSPITAL_COD`, `SALA_COD`, `EMPLEAT_NO` y `COGNOM` sobre la tabla `PLANTILLA`. 

```sql
-- Muestre los trabajadores (código hospital, código sala, número empleado y apellido) existentes.
USE sanitat;
SELECT HOSPITAL_COD AS 'Código Hospital', SALA_COD AS 'Código Sala', EMPLEAT_NO AS 'Número empleado', COGNOM FROM PLANTILLA;
```

## Query 4
En este código selecciono los atributos: `HOSPITAL_COD`, `SALA_COD`, `EMPLEAT_NO` y `COGNOM` sobre la tabla `PLANTILLA`. Y posteriormente realizar un filtro para que salgan todos menos los que su turno sea `N` (Nocturno).

```sql
-- Muestre los trabajadores (código hospital, código sala, número empleado y apellido) que no sean del turno de noche.
USE sanitat;
SELECT HOSPITAL_COD AS 'Código Hospital', SALA_COD AS 'Código Sala', EMPLEAT_NO AS 'Número empleado', COGNOM FROM PLANTILLA
	WHERE TORN != 'N';
```

## Query 5
En esta consulta selecciono todos los atributos sobre la tabla `MALALT` y realizo un filtro sobre `DATA_NAIX` con la función `YEAR()` para sacar únicamente los que hayan nacido en el año 1960.

```sql
-- Muestre a los enfermos nacidos en 1960.
USE sanitat;
SELECT * FROM MALALT
	WHERE YEAR(DATA_NAIX) = 1960;
```

## Query 6
Esta consulta es similar a la anterior pero sacando todos los que hayan nacido a partir del año 1960, para ello he usado `>=`.

```sql
-- Muestre a los enfermos nacidos a partir del año 1960.
USE sanitat;
SELECT * FROM MALALT
	WHERE YEAR(DATA_NAIX) >= 1960;
```