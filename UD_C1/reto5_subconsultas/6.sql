-- Obtener los álbumes con un número de canciones superiores a la media.
USE Chinook;
SELECT * FROM Album
WHERE AlbumId IN (
SELECT AlbumId
FROM Track
GROUP BY AlbumId
HAVING COUNT(*) > (SELECT AVG(N_Canciones) FROM
(
SELECT AlbumId,Count(*) AS N_Canciones
FROM Track
GROUP BY AlbumId
) AS Album_NCanciones));