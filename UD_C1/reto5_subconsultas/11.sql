-- Obtener empleados y el número de clientes a los que sirve cada uno de ellos.
USE Chinook;
SELECT EmployeeId, FirstName, LastName, (
SELECT COUNT(*) AS N_Customers
FROM Customer
WHERE Customer.SupportRepId = Employee.EmployeeId
) AS N_Customers
FROM Employee;