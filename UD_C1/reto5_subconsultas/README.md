# Reto 5 Consultas con subconsultas

Adrián López Pascual

En este reto trabajamos con la base de datos `Chinook` , que nos la descargamos desde gitlab [[1]](#links)] . A continuación comenzaremos a prácticar las consultas usando todo lo aprendido con anterioridad y las `subconsultas`.

* [Enlace al repo](https://gitlab.com/Byadrian31/bases-de-datos)
* [Enlace al ejercicio](https://gitlab.com/Byadrian31/bases-de-datos/-/tree/main/UD_C1/reto5_subconsultas?ref_type=heads)


## Query 1
Para esta consulta, selecciono `Name` y `Milliseconds` de la tabla `Track`, y filtro por `Milliseconds` que sean mayores que la subconsulta que me saca la media de `Milliseconds` de la tabla `Track`:

```sql
-- Obtener las canciones con una duración superior a la media.
USE Chinook;
SELECT Name, Milliseconds FROM Track
	WHERE Milliseconds > (SELECT AVG(Milliseconds) FROM Track);
```


## Query 2
Lo selecciono todo de la tabla `Invoice` y filtro por `CustomerId` siendo igual que el resultado de la subconsulta donde selecciono `CustomerId` de la tabla `Customer` donde el campo `Email` sea `emma_jones@hotmail.com`, fuera de la subconsulta ordeno por `InvoiceId` de manera descendente y limito a 5 los valores que saldrán en el resultado:

```sql
-- Listar las 5 últimas facturas del cliente cuyo email es "emma_jones@hotmail.com".
USE Chinook;
SELECT * FROM Invoice
	WHERE CustomerId = (SELECT CustomerId FROM Customer WHERE Email = "emma_jones@hotmail.com")
	ORDER BY InvoiceId DESC
	LIMIT 5;
```


## Query 3
En esta consulta, selecciono todo de `Playlist`, y filtro donde la `PlaylistId` se encuentre dentro de la subconsulta donde busco las `PlaylistId` de la tabla `PlaylistTrack` donde `TrackId` se encuentre dentro de la subconsulta donde pido `TrackId` de la tabla `Track` y que `GenreId` sea igual que el resultado de la subconsulta donde busco el `GenreId` de la tabla `Genre` y el `Name` sea `Reggae` :

```sql
-- Mostrar las listas de reproducción en las que hay canciones de reggae.
USE Chinook;
SELECT * FROM Playlist
WHERE PlaylistId IN (
 SELECT PlaylistId FROM PlaylistTrack
 WHERE TrackId IN (
  SELECT TrackId FROM Track
  WHERE GenreId = (
   SELECT GenreId FROM Genre
   WHERE Name = "Reggae"
   )
  )
 );
```

## Query 4
Para realizar la siguiente consulta, he decidido seleccionar  `CustomerId`,`FirstName` y `LastName` de la tabla `Customer` donde filtro indicando que `CustomerId` se encuentre dentro de la subconsulta donde selecciono `CustomerId` de la tabla `Invoice` donde el `Total` es mayor que 20:

```sql
-- Obtener la información de los clientes que han realizado compras superiores a 20€.
USE Chinook;
SELECT CustomerId,FirstName,LastName FROM Customer
WHERE CustomerId IN (
SELECT CustomerId FROM Invoice
WHERE Total > 20
);
```

## Query 5
En esta consulta selecciono `a.AlbumId`, `a.Title`y `ar.Name` de la tabla `Album(a)` y realizo un `JOIN` con la tabla `Artist(ar)` y las filtro indicando que el campo `a.ArtistId` es igual que `ar.ArtistId`, una vez realizado el `JOIN` filtro donde `a.AlbumId` se encuentre dentro de la subconsulta donde selecciono `AlbumId` de la tabla `Track` agrupando por `AlbumId` y cojo esa agrupacion con `HAVING COUNT(*)` que sea mayor que 15 y fuera de la consulta ordeno por `a.AlbumId` :

```sql
-- Álbumes que tienen más de 15 canciones, junto a su artista.
USE Chinook;
SELECT a.AlbumId, a.Title, ar.Name FROM Album a
JOIN Artist ar 
ON a.ArtistId = ar.ArtistId
WHERE a.AlbumId IN (
    SELECT AlbumId FROM Track
    GROUP BY AlbumId
	HAVING COUNT(*) > 15
    )
ORDER BY a.AlbumId;
```

## Query 6
Selecciono todo de la tabla `Album` donde `AlbumId` se encuentre dentro de la subconsulta donde selecciono el `AlbumId` de la tabla `Track` agrupando por `AlbumId` y cojo esa agrupación con `HAVING COUNT(*)` que sea mayor que el valor de la subconsulta donde saco la media de `N_Canciones` de la tabla que he creado realizando una subconsulta donde selecciono `AlbumId` y `Count(*)(referenciado como N_Canciones)` de la tabla `Track` agrupandolas por `AlbumId` finalmente referenciando esta tabla como `Album_NCanciones` :

```sql
-- Obtener los álbumes con un número de canciones superiores a la media.
USE Chinook;
SELECT * FROM Album
WHERE AlbumId IN (
SELECT AlbumId
FROM Track
GROUP BY AlbumId
HAVING COUNT(*) > (SELECT AVG(N_Canciones) FROM
(
SELECT AlbumId,Count(*) AS N_Canciones
FROM Track
GROUP BY AlbumId
) AS Album_NCanciones));
```


## Query 7
Selecciono todo de la tabla `Album` donde `AlbumId` se encuentre dentro de la subconsulta donde selecciono el `AlbumId(T_dur)` de la tabla `Track` agrupando por `AlbumId` y cojo esa agrupación sumándola con `HAVING SUM(Milliseconds)` que sea mayor que el valor de la subconsulta donde saco la media de `T_dur` de la tabla que he creado realizando una subconsulta donde selecciono `AlbumId` y `SUM(Milliseconds)(referenciado como T_dur)` de la tabla `Track` agrupandolas por `AlbumId` finalmente referenciando esta tabla como `Album_TDur` :

```sql
-- Obtener los álbumes con una duración total superior a la media.
USE Chinook;
SELECT * FROM Album
WHERE AlbumId IN (
SELECT AlbumId AS T_dur
FROM Track
GROUP BY AlbumId
HAVING SUM(Milliseconds) > (SELECT AVG(T_dur) FROM
(
SELECT AlbumId,SUM(Milliseconds) AS T_dur
FROM Track
GROUP BY AlbumId
) AS Album_TDur));
```

## Query 8
Para realizar esta consulta, selecciono `GenreId`, `TrackId`, `Name` y `AlbumId` de la tabla `Track` donde `GenreId` sea igual que el resultado de la subconsulta donde selecciono `GenreId` de la tabla que he creado con subconsulta donde selecciono `GenreId` y `COUNT(*)(referenciado como n_canciones)` de la tabla `Track` agrupando por `GenreId`, ordenando por `COUNT(*)` de manera descendente, limitando todo a 1, referenciando la tabla como `subconsulta`:

```sql
-- Canciones del género con más canciones.
USE Chinook;
SELECT GenreId, TrackId, Name, AlbumId FROM Track
WHERE GenreId = (
    SELECT GenreId
    FROM (
        SELECT GenreId, COUNT(*) AS n_canciones
        FROM Track
        GROUP BY GenreId
        ORDER BY COUNT(*) DESC
        LIMIT 1
    ) AS subconsulta
);
```

## Query 9
En esta consulta selecciono `t.GenreId`, `t.TrackId`, `t.Name`, `t.AlbumId` de la tabla `Track(t)` realizo un `JOIN` sobre la tabla `PlaylistTrack(pt)` y las junto indicando que los valores `t.TrackId` y `pt.TrackId` son iguales, una vez realizado el `JOIN` filtro por `pt.PlaylistId` que sea igual que el resultado de la subconsulta donde selecciono `PlaylistId` de la tabla que he creado con una subconsulta donde selecciono `PlaylistId` y `COUNT(*)(referenciado como n_canciones)` de la tabla `PlaylistTrack` agrupando por `PlaylistId`, ordenando por `COUNT(*)` de manera descendente, limitando todo a 1, referenciando la tabla como `subconsulta`:

```sql
-- Canciones de la playlist con más canciones.
USE Chinook;
SELECT t.GenreId, t.TrackId, t.Name, t.AlbumId
FROM Track AS t
JOIN PlaylistTrack as pt
ON t.TrackId = pt.TrackId
WHERE pt.PlaylistId = (
    SELECT PlaylistId
    FROM (
        SELECT PlaylistId, COUNT(*) AS n_canciones
        FROM PlaylistTrack
        GROUP BY PlaylistId
        ORDER BY COUNT(*) DESC
        LIMIT 1
    ) AS subconsulta
);

```

## Query 10
Para realizar esta consulta selecciono `CustomerId`, `FirstName`, `LastName`, y el resultado de la subconsulta donde selecciono la suma de `Total` `SUM(Total)` de la tabla `Invoice` donde indico que los valores `Customer.CustomerId` y `Invoice.CustomerId` son iguales y finalmente referencio esta subconsulta como `TotalGastado` de la tabla `Customer` ordenado por `CustomerId` de manera ascendente:

```sql
-- Muestra los clientes junto con la cantidad total de dinero gastado por cada uno en compras
USE Chinook;
SELECT CustomerId, FirstName, LastName,
    (
        SELECT SUM(Total)
        FROM Invoice
        WHERE Customer.CustomerId = Invoice.CustomerId
    ) AS TotalGastado
FROM Customer
ORDER BY CustomerId ASC;
```

## Query 11(Primera manera)
Para realizar esta consulta, selecciono las columnas `EmployeeId`, `FirstName`, `LastName` y también el resultado de la subconsulta donde selecciono `COUNT(*)` de la tabla `Customer` donde indico que los valores `Customer.SupportRepId` y `Employee.EmployeeId` son iguales y finalmente referencio esta subconsulta como `N_Customers` y una vez fuera de la consulta indico que la tabla del SELECT es `Employee`:

```sql
-- Obtener empleados y el número de clientes a los que sirve cada uno de ellos.
USE Chinook;
SELECT EmployeeId, FirstName, LastName, (
SELECT COUNT(*) AS N_Customers
FROM Customer
WHERE Customer.SupportRepId = Employee.EmployeeId
) AS N_Customers
FROM Employee;
```

## Query 11(Segunda manera)
He vuelto a realizar esta consulta de otra manera para demostrar que hay más de una manera de hacer las consultas, para poder llevar a cabo esta consulta he seleccionado `EmployeeId`,`FirstName`,`LastName`, `N_Customers`(este último campo se saca de una subconsulta posterior), de la tabla `Employee` y realizo un `LEFT JOIN` sobre la tabla que he creado con la subconsulta donde selecciono `SupportRepId`, `COUNT(*)` referenciado como `N_Customers` de la tabla `Customer` y agrupar por `SupporRepId` referenciando esta tabla como `Empleado_NCustomers` y junto el `LEFT JOIN` indicando que  `Employee.EmployeeId` y `Empleado_NCustomers.SupportRepId` son iguales:

```sql
-- Obtener empleados y el número de clientes a los que sirve cada uno de ellos.
USE Chinook;
SELECT EmployeeId,FirstName,LastName, N_Customers
FROM Employee
LEFT JOIN (
SELECT SupportRepId, COUNT(*) AS N_Customers
FROM Customer
GROUP BY SupportRepId
) AS Empleado_NCustomers
ON Employee.EmployeeId = Empleado_NCustomers.SupportRepId;
```

## Query 12
En esta consulta selecciono `EmployeeId`,`FirstName`, `Lastname`, y el resultado de una subconsulta donde sumo la columna `i.Total` de la tabla `Invoice(i)`, realizo un `JOIN` sobre la tabla `Customer(c)` indicando que `i.CustomerId` y `c.CustomerId` son iguales, para posteriormente filtrar indicando que `c.SupportRepId` y `Employee.EmployeeId` son el mismo valor, cierro la subconsulta y la referencio como `ventas_totales`. Todos los valores que selecciono es sobre la tabla `Employee` para finalmente ordenar por la columna de la subconsulta(`ventas_totales`) de manera descendente:

```sql
-- Ventas totales de cada empleado
USE Chinook;
SELECT EmployeeId,FirstName, Lastname, (
SELECT SUM(i.Total) FROM Invoice AS i
JOIN Customer c
ON i.CustomerId = c.CustomerId
WHERE c.SupportRepId = Employee.EmployeeId
) AS ventas_totales 
FROM Employee
ORDER BY ventas_totales DESC;
```

## Query 13
Para realizar esta consulta, selecciono las columnas `AlbumId`, `Title` y el resultado de la subconsulta donde selecciono `COUNT(*)` de la tabla `Track` donde indico que los valores `Track.AlbumId` y `Album.AlbumId` son iguales y finalmente referencio esta subconsulta como `n_canciones` y una vez fuera de la consulta indico que la tabla del SELECT es `Album` :

```sql
-- Álbumes junto a número de canciones en cada uno
USE Chinook;
SELECT AlbumId, Title, (
SELECT COUNT(*) FROM Track
WHERE Track.AlbumId = Album.AlbumId
) AS n_canciones
FROM Album;
```

## Query 14
Para realizar esta consulta, selecciono las columnas `Name` y el resultado de la subconsulta donde selecciono `Title` de la tabla `Album` donde indico que los valores `Artist.ArtistId` y `Album.ArtistId` son iguales, ordeno por `AlbumId` de manera descendente y limito a un solo valor(`LIMIT 1`), finalmente referencio esta subconsulta como `Album_reciente` y una vez fuera de la consulta indico que la tabla del SELECT es `Artist` :

```sql
-- Obtener el nombre del álbum más reciente de cada artista.
USE Chinook;
SELECT Name, (
SELECT Title FROM Album
WHERE Artist.ArtistId = Album.ArtistId
ORDER BY AlbumId DESC
LIMIT 1
) AS Album_reciente
FROM Artist;
```

## Links
[1] https://github.com/lerocha/chinook-database/