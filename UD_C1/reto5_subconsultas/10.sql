-- Muestra los clientes junto con la cantidad total de dinero gastado por cada uno en compras
USE Chinook;
SELECT CustomerId, FirstName, LastName,
    (
        SELECT SUM(Total)
        FROM Invoice
        WHERE Customer.CustomerId = Invoice.CustomerId
    ) AS TotalGastado
FROM Customer
ORDER BY CustomerId ASC;