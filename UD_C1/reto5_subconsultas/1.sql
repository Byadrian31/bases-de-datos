-- Obtener las canciones con una duración superior a la media.
USE Chinook;
SELECT Name, Milliseconds FROM Track
	WHERE Milliseconds > (SELECT AVG(Milliseconds) FROM Track);