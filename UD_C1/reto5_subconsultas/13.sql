-- Álbumes junto a número de canciones en cada uno
USE Chinook;
SELECT AlbumId, Title, (
SELECT COUNT(*) FROM Track
WHERE Track.AlbumId = Album.AlbumId
) AS n_canciones
FROM Album;