-- Canciones de la playlist con más canciones.
USE Chinook;
SELECT t.GenreId, t.TrackId, t.Name, t.AlbumId
FROM Track AS t
JOIN PlaylistTrack as pt
ON t.TrackId = pt.TrackId
WHERE pt.PlaylistId = (
    SELECT PlaylistId
    FROM (
        SELECT PlaylistId, COUNT(*) AS n_canciones
        FROM PlaylistTrack
        GROUP BY PlaylistId
        ORDER BY COUNT(*) DESC
        LIMIT 1
    ) AS subconsulta
);
