-- Ventas totales de cada empleado
USE Chinook;
SELECT EmployeeId,FirstName, Lastname, (
SELECT SUM(i.Total) FROM Invoice AS i
JOIN Customer c
ON i.CustomerId = c.CustomerId
WHERE c.SupportRepId = Employee.EmployeeId
) AS ventas_totales 
FROM Employee
ORDER BY ventas_totales DESC;