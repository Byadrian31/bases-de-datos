-- Obtener el nombre del álbum más reciente de cada artista.
USE Chinook;
SELECT Name, (
SELECT Title FROM Album
WHERE Artist.ArtistId = Album.ArtistId
ORDER BY AlbumId DESC
LIMIT 1
) AS Album_reciente
FROM Artist;