-- Canciones del género con más canciones.
USE Chinook;
SELECT GenreId, TrackId, Name, AlbumId FROM Track
WHERE GenreId = (
    SELECT GenreId
    FROM (
        SELECT GenreId, COUNT(*) AS n_canciones
        FROM Track
        GROUP BY GenreId
        ORDER BY COUNT(*) DESC
        LIMIT 1
    ) AS subconsulta
);