-- Muestra el número de canciones de cada género (deberá mostrarse el nombre del género).
USE Chinook;
SELECT COUNT(t.TrackId) AS "Canciones", g.Name
FROM Track as t
JOIN Genre as g
ON t.GenreId = g.GenreId
GROUP BY g.Name
ORDER BY g.Name;