-- Muestra el importe medio, mínimo y máximo de cada factura.
USE Chinook;
SELECT AVG(Total) AS "Media",
MIN(Total) AS "Mínimo",
MAX(Total) AS "Máximo" 
FROM Invoice;