-- Muestra el número de canciones del álbum “Out Of Time”.
SELECT COUNT(t.TrackId) AS "Canciones", a.Title
FROM Track AS t
JOIN Album AS a
ON t.AlbumId = a.AlbumId
WHERE a.Title = "Out Of Time"