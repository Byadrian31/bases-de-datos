-- Muestra todas las canciones compuestas por AC/DC.
USE Chinook;
SELECT Name AS "Cancion" FROM Track
WHERE Composer = "AC/DC";