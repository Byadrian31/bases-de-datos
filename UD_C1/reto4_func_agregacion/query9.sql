-- Muestra todas las facturas de los clientes berlineses. Deberán mostrarse las colum-
-- nas: fecha de la factura, nombre completo del cliente, dirección de facturación,
-- código postal, país, importe (en este orden).

USE Chinook;
SELECT f.InvoiceDate AS "Fecha factura", c.FirstName AS "Nombre", c.Lastname AS "Apellido",
f.BillingAddress AS "Dirección de facturación", c.PostalCode AS "Código postal",
c.Country AS "País", f.Total AS "Importe"
FROM Invoice AS f
JOIN Customer AS c
ON f.CustomerId = c.CustomerId
WHERE c.City = "Berlin";