-- Muestra los países en los que tenemos al menos 5 clientes.
USE Chinook;
SELECT COUNT(CustomerID), Country
FROM Customer
GROUP BY Country
HAVING COUNT(CustomerID) >= 5;