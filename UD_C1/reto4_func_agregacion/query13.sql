-- Muestra el número total de artistas.
USE Chinook;
Select COUNT(ArtistId) AS "Nº Artistas" FROM Artist;