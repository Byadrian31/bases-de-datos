-- Muestra las 10 canciones que más tamaño ocupan.
USE Chinook;
SELECT Name AS "Cancion" FROM Track
ORDER BY Bytes DESC
LIMIT 10;