# Reto 2.1 Consultas con funciones de agregación

Adrián López Pascual

En este reto trabajamos con la base de datos `Chinook` , que nos la descargamos desde gitlab [[1](#links)] . A continuación comenzaremos a prácticar las consultas usando `JOIN` y `funciones de agregación`.

* [Enlace al repo](https://gitlab.com/Byadrian31/bases-de-datos)
* [Enlace al ejercicio](https://gitlab.com/Byadrian31/bases-de-datos/-/tree/main/UD_C1/reto4_func_agregacion?ref_type=heads)


## Query 1
Para sacar todos los clientes de Francia, selecciono las columnas `FirstName` y `LastName`(poniéndole un ALIAS a cada una) de la tabla `Customer`, y realizo un filtro sobre la columna `Country` que equivalga a `France`:

```sql
-- Encuentra todos los clientes de Francia
USE Chinook;
SELECT FirstName AS "Nombre",
LastName AS "Apellido"
FROM Customer
WHERE Country = "France";
```


## Query 2
Para sacar todos las facturas del primer trimestre del año, selecciono las columnas `InvoiceId` y `InvoiceDate` (poniéndole un ALIAS a cada una) de la tabla `Invoice`, y realizo un filtro sobre la columna `InvoiceDate` usando la función `YEAR()` para únicamente el año para decir que sea igual que el año actual (`YEAR(now())`), con esto sacamos que todas las facturas sean del año actual, y ahora saco el mes (`Month()`) de la columna `InvoiceDate` y le indico que el mes esté entre `1` y `3` con la función `BETWEEN`:

```sql
-- Muestra las facturas del primer trimestre de este año.
USE Chinook;
SELECT InvoiceId AS "Factura", InvoiceDate AS "Fecha" FROM Invoice
WHERE YEAR(InvoiceDate) = YEAR(now())
AND MONTH(InvoiceDate) BETWEEN 1 AND 3 ;
```


## Query 3
Para sacar las canciones del grupo AC/DC, selecciono la columna `Name`(dándole un ALIAS) de la tabla `Track`, y creo un filtro sobre la columna `Composer` para que equivalga a `AC/DC`:

```sql
-- Muestra todas las canciones compuestas por AC/DC.
USE Chinook;
SELECT Name AS "Cancion" FROM Track
WHERE Composer = "AC/DC";
```

## Query 4
Para mostrar las 10 canciones que más tamaño ocupan, selecciono la columna `Name`(dándole un ALIAS) de la tabla `Track`, y ordeno (`ORDER BY`) `Bytes` de manera descendente `DESC` y limito para que solo salgan 10 valores `LIMIT 10`:
```sql
-- Muestra las 10 canciones que más tamaño ocupan.
USE Chinook;
SELECT Name AS "Cancion" FROM Track
ORDER BY Bytes DESC
LIMIT 10;
```

## Query 5
Para ver todos los paises donde hay clientes, selecciono la columna `Country` con un `DISTINCT` para que no se me repitan los países de la tabla `Customer` y creo un filtro sobre la columna `Country` para todos aquellos valores que no sean nulo (`IS NOT NULL`):

```sql
-- Muestra el nombre de aquellos países en los que tenemos clientes.
USE Chinook;
SELECT DISTINCT(Country) FROM Customer
WHERE Country IS NOT NULL;
```

## Query 6
Para mostrar todos los géneros musicales, selecciono la columna `Name`(proporcionando un ALIAS) de la tabla `Genre`:

```sql
-- Muestra todos los géneros musicales.
USE Chinook;
SELECT Name AS "Nombre" FROM Genre;
```

### COMENZAMOS CON JOIN

## Query 7
Para mostrar todos los artistas y sus álbumes, selecciono la columna `Name`(proporcionando un ALIAS) de la tabla `Artista` y la columna `Title` de la tabla `Album`, realizo un `JOIN` de la tabla `Artista(ar)` con `Album(al)` comparandolas (`ON`) sobre las columnas `ar.ArtistId = al.ArtistId` para finalmente ordenar por `ar.Name` :

```sql
-- Muestra todos los artistas junto a sus álbumes.
USE Chinook;
SELECT ar.Name AS "Artista", al.Title AS "Álbum"
FROM Artist AS ar
JOIN Album AS al
ON ar.ArtistId = al.ArtistId
ORDER BY ar.Name;
```

## Query 8
Para realizar esta consulta, selecciono las columnas `FirstName`, `BirthDate` y de nuevo `Firstname` (proporcionando ALIAS a `Firstname`) de la tabla `Employee` y realizo un `JOIN` sobre la misma tabla `Employee` dándole los ALIAS `emp` y `sup` sobre las columnas (`ON`) `emp.ReportsTo = sup.EmployeeId`, ordeno por `emp.BirthDate` de manera descendente `DESC` y finalmente limitándolo por 5 valores (`LIMIT 5`):

```sql
-- Muestra los nombres de los 5 empleados más jóvenes junto a los nombres de sus supervisores, si los tienen
USE Chinook;
SELECT emp.FirstName AS "Empleado", emp.BirthDate, sup.Firstname AS "Supervisor" 
FROM Employee AS emp
JOIN Employee AS sup
ON emp.ReportsTo = sup.EmployeeId
ORDER BY emp.BirthDate DESC
LIMIT 5;
```

## Query 9
Para realizar la siguiente consulta, selecciono las columnas `InvoiceDate`, `BillingAddress` y `Total`(proporcionando un ALIAS a cada una) de la tabla `Invoice` y además también selecciono las columnas `Firstname`, `Lastname`, `PostalCode` y `Country` de la tabla `Customer`(proporcionando un ALIAS a cada una) y realizo un `JOIN` de la tabla `Invoice(f)` con `Customer(c)` comparandolas (`ON`) sobre las columnas `f.CustomerId = c.CustomerId` y creo un filtro sobre la columna `c.City` para todos aquellos valores que  sean (`Berlin`) :

```sql
-- Muestra todas las facturas de los clientes berlineses. Deberán mostrarse las colum-
-- nas: fecha de la factura, nombre completo del cliente, dirección de facturación,
-- código postal, país, importe (en este orden).

USE Chinook;
SELECT f.InvoiceDate AS "Fecha factura", c.FirstName AS "Nombre", c.Lastname AS "Apellido",
f.BillingAddress AS "Dirección de facturación", c.PostalCode AS "Código postal",
c.Country AS "País", f.Total AS "Importe"
FROM Invoice AS f
JOIN Customer AS c
ON f.CustomerId = c.CustomerId
WHERE c.City = "Berlin";
```

## Query 10
Para realizar la siguiente consulta, selecciono las columnas `Name`(proporcionando un ALIAS) de la tabla `Playlist`, columnas `Name` y `Milliseconds` (proporcionando ALIAS) y  columna `Title` de la tabla `Album`(proporcionando un ALIAS) y realizo los `JOIN` necesarios de las tablas `Playlist(List)` , `PlaylistTrack(pt)` , `Track(t)`  y `Album(a)`  comparandolas (`ON`) sobre las columnas:
`f.CustomerId = c.CustomerId` , `pt.TrackId = t.TrackId` y `t.AlbumId = a.AlbumId` y creo un filtro sobre la columna `List.Name` para todos aquellos valores que empiecen por la letra C (`LIKE "C%"`) y finalmente ordeno (`ORDER BY`) por `t.AlbumId, t.Milliseconds Desc` :

```sql
-- Muestra las listas de reproducción cuyo nombre comienza por C, junto a todas
-- sus canciones, ordenadas por álbum y por duración.
USE Chinook;
SELECT List.Name AS "PlayList", t.Name AS "Cancion", a.Title AS "Álbum", t.Milliseconds AS "Duración"
FROM Playlist AS List
JOIN PlaylistTrack AS pt
JOIN Track AS t
JOIN Album AS a
ON List.PlaylistId = pt.PlaylistId
AND pt.TrackId = t.TrackId
AND t.AlbumId = a.AlbumId
WHERE List.Name LIKE "C%"
ORDER BY t.AlbumId, t.Milliseconds DESC;
```

## Query 11
Para realizar la siguiente consulta, selecciono las columnas `FirstName` y `LastName` (proporcionando un ALIAS a cada una) de la tabla `Customer` y la columna `Total` (proporcionando un ALIAS) de la tabla `Invoice` y realizo un `JOIN` sobre las tablas `Customer(c)` y `Invoice(i)` sobre las columnas(`ON`) `c.CustomerId = i.CustomerId`, creando un filtro sobre la columa `Total` para todos aquellos valores que sean `> 10` y finalmente ordeno `ORDER BY c.LastName DESC` :

```sql
-- Muestra qué clientes han realizado compras por valores superiores a 10€, ordenados por apellido.
USE Chinook;
SELECT c.Firstname AS "Nombre", c.Lastname AS "Apellido", i.Total AS "Importe"
FROM Customer AS c
JOIN Invoice AS i
ON c.CustomerId = i.CustomerId
WHERE i.Total > 10
ORDER BY c.LastName DESC;
```
### COMENZAMOS CON FUNCIONES DE AGREGACIÓN

## Query 12
Para realizar la siguiente consulta voy a usar las funciones `AVG(media)`, `MIN(mínimo)` y `MAX(máximo)`. Selecciono la columna `Total` usando cada función de agregación mencionadas anteriormente (proporcionando un ALIAS a cada una) de la tabla `Invoice`:

```sql
-- Muestra el importe medio, mínimo y máximo de cada factura.
USE Chinook;
SELECT AVG(Total) AS "Media",
MIN(Total) AS "Mínimo",
MAX(Total) AS "Máximo" 
FROM Invoice;
```

## Query 13
Para realizar la siguiente consulta, selecciono la columna `Name` con un `COUNT()`(proporcionando un ALIAS) de la tabla `Artist`:

```sql
-- Muestra el número total de artistas.
USE Chinook;
Select COUNT(ArtistId) AS "Nº Artistas" FROM Artist;
```

## Query 14
Para realizar la siguiente consulta, selecciono la columna `Name` con un `COUNT()`(proporcionando un ALIAS) de la tabla `Track` y la columna `Title` de la tabla `Album` realizo un `JOIN` de las tablas `Track(t)` y `Album(a)` sobre las columnas(`ON`) `t.AlbumId = a.AlbumId` y hago un filtro sobre la columna `a.Title` para que todos los valores sean `= "Out Of Time"`:

```sql
-- Muestra el número de canciones del álbum “Out Of Time”.
SELECT COUNT(t.TrackId) AS "Canciones", a.Title
FROM Track AS t
JOIN Album AS a
ON t.AlbumId = a.AlbumId
WHERE a.Title = "Out Of Time"
```

## Query 15
Para realizar la siguiente consulta, selecciono la columna `Country` con la función `COUNT` (proporcionando un ALIAS) de la tabla `Customer` y hago un filtro sobre la columna `Country` para todos aquellos valores que no sean nulos (`IS NOT NULL`):

```sql
-- Muestra el número de países donde tenemos clientes.
USE Chinook;
SELECT COUNT(Country) AS "Nº Paises" FROM Customer
WHERE Country IS NOT NULL;
```

## Query 16
Para realizar la siguiente consulta, selecciono la columna `TrackId` con la función `COUNT` (proporcionando un ALIAS) de la tabla `Track` y la columna `Name` (proporcionando un ALIAS) de la tabla `Genre`, realizo un `JOIN` sobre las columnas(`ON`) `t.GenreId = g.GenreId`, agrupo por `g.Name` (`GROUP BY g.Name`) y ordeno por `g.Name` (`ORDER BY g.Name`):

```sql
-- Muestra el número de canciones de cada género (deberá mostrarse el nombre del género).
USE Chinook;
SELECT COUNT(t.TrackId) AS "Canciones", g.Name
FROM Track as t
JOIN Genre as g
ON t.GenreId = g.GenreId
GROUP BY g.Name
ORDER BY g.Name;
```

## Query 17
Para realizar la siguiente consulta, selecciono la columna `Title`(proporcionando un ALIAS) de la tabla `Album` y la columna `TrackId` con la función `COUNT` (proporcionando un ALIAS) de la tabla `Track`, realizo un `JOIN` sobre las tablas `Album(a)` y `Track(t)` sobre las columnas(`ON`), agrupando por `a.Title`(`GROUP BY a.Title`) y ordeno `ORDER BY COUNT(t.trackID) DESC`:

```sql
-- Muestra los álbumes ordenados por el número de canciones que tiene cada uno.
SELECT a.Title AS "Álbum", COUNT(t.TrackId) AS "Canciones"
FROM Album AS a
JOIN Track AS t
ON a.AlbumId = t.AlbumId
GROUP BY a.Title
ORDER BY COUNT(t.trackID) DESC;
```

## Query 18 
Para realizar la siguiente consulta, selecciono la columna `Name`(proporcionando un ALIAS) de la tabla `Genre`, y la columna `Quantity`(proporcionando un ALIAS) de la tabla `InvoiceLine`, realizo un `JOIN` sobre las tablas `Genre(g)` , `Track(t)` y `InvoiceLine(i)` sobre las columnas(`ON`) `g.GenreId = t.GenreId` y `t.TrackId = i.TrackId` agrupo por `g.Name` (`GROUP BY g.Name`) y finalmente ordeno `ORDER BY COUNT(i.Quantity) DESC` :

```sql
-- Encuentra los géneros musicales más populares (los más comprados).
USE Chinook;
SELECT g.Name AS "Genero", COUNT(i.Quantity) AS "Total compras" 
FROM Genre AS g
JOIN Track AS t
JOIN InvoiceLine AS i
ON g.GenreId = t.GenreId
AND t.TrackId = i.TrackId
GROUP BY g.Name
ORDER BY COUNT(i.Quantity) DESC;
```

## Query 19
Para realizar la siguiente consulta, selecciono la columna `Title`(proporcionando un ALIAS) de la tabla `Album`, y la columna `Quantity`(proporcionando un ALIAS) de la tabla `InvoiceLine`, realizo un `JOIN` sobre las tablas `Album(a)` , `Track(t)` y `InvoiceLine(i)` sobre las columnas(`ON`) `a.AlbumId = t.AlbumId` y `t.TrackId = i.TrackId` agrupo por `a.Title` (`GROUP BY a.title`), ordeno `ORDER BY COUNT(i.Quantity) DESC` y finalmente limito solo a 6 valores (`LIMIT 6`) :

```sql
-- Lista los 6 álbumes que acumulan más compras.
SELECT a.Title AS "Género", COUNT(i.Quantity) AS "Total compras" 
FROM Album AS a
JOIN Track AS t
JOIN InvoiceLine AS i
ON a.AlbumId = t.AlbumId
AND t.TrackId = i.TrackId
GROUP BY a.Title
ORDER BY COUNT(i.Quantity) DESC
LIMIT 6;
```

## Query 20
Para realizar la siguiente consulta, selecciono la columna `CustomerID` con la función `COUNT` y `Country` (proporcionando un ALIAS a cada una) de la tabla `Customer`, agrupando por `Country` (`GROUP BY Country`) y finalmente uso `HAVING`(para recoger los valores del `GROUP BY`) con `COUNT(CustomerID) >= 5` para sacar al menos 5 valores:

```sql
-- Muestra los países en los que tenemos al menos 5 clientes.
USE Chinook;
SELECT COUNT(CustomerID), Country
FROM Customer
GROUP BY Country
HAVING COUNT(CustomerID) >= 5;
```

## Links
[1] https://github.com/lerocha/chinook-database/