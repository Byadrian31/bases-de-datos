-- Encuentra los géneros musicales más populares (los más comprados).
USE Chinook;
SELECT g.Name AS "Genero", COUNT(i.Quantity) AS "Total compras" 
FROM Genre AS g
JOIN Track AS t
JOIN InvoiceLine AS i
ON g.GenreId = t.GenreId
AND t.TrackId = i.TrackId
GROUP BY g.Name
ORDER BY COUNT(i.Quantity) DESC;