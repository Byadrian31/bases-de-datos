-- Muestra qué clientes han realizado compras por valores superiores a 10€, ordenados por apellido.
USE Chinook;
SELECT c.Firstname AS "Nombre", c.Lastname AS "Apellido", i.Total AS "Importe"
FROM Customer AS c
JOIN Invoice AS i
ON c.CustomerId = i.CustomerId
WHERE i.Total > 10
ORDER BY c.LastName DESC;