-- Muestra las facturas del primer trimestre de este año.
USE Chinook;
SELECT InvoiceId AS "Factura", InvoiceDate AS "Fecha" FROM Invoice
WHERE YEAR(InvoiceDate) = YEAR(now())
AND MONTH(InvoiceDate) BETWEEN 1 AND 3 ;