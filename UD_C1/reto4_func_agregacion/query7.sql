-- Muestra todos los artistas junto a sus álbumes.
USE Chinook;
SELECT ar.Name AS "Artista", al.Title AS "Álbum"
FROM Artist AS ar
JOIN Album AS al
ON ar.ArtistId = al.ArtistId
ORDER BY ar.Name;