-- Muestra los nombres de los 5 empleados más jóvenes junto a los nombres de sus supervisores, si los tienen
USE Chinook;
SELECT emp.FirstName AS "Empleado", emp.BirthDate, sup.Firstname AS "Supervisor" 
FROM Employee AS emp
JOIN Employee AS sup
ON emp.ReportsTo = sup.EmployeeId
ORDER BY emp.BirthDate DESC
LIMIT 5;