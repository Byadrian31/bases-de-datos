-- Encuentra todos los clientes de Francia
USE Chinook;
SELECT FirstName AS "Nombre",
LastName AS "Apellido"
FROM Customer
WHERE Country = "France";