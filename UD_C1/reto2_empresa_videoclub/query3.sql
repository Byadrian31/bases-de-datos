-- Muestre el código, nombre, área y teléfono de los clientes de la empresa.
USE empresa;
SELECT 
CLIENT_COD AS "Código cliente",
NOM AS "Nombre",
AREA AS "Área",
TELEFON AS "Teléfono"
FROM client;