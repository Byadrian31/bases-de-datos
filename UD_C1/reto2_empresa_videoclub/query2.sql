-- Muestre los productos (código y descripción) que contienen la palabra tenis en la descripción.
USE empresa;
SELECT 
PROD_NUM AS "Código",
DESCRIPCIO AS "Descripción"
FROM producte
WHERE DESCRIPCIO LIKE "%TENNIS%";