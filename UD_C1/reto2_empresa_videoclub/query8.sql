-- Lista de productos (descripción) facturados en la factura número 3.
USE videoclub;
SELECT
Descripcio AS "Descripción"
FROM  detallfactura
WHERE LiniaFactura = 3;