-- Muestre los productos (código y descripción) que comercializa la empresa.
USE empresa;
-- SELECT * FROM producte;
SELECT 
PROD_NUM AS "Código",
DESCRIPCIO AS "Descripción"
FROM producte;