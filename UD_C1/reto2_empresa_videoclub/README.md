# Reto 2: Consultas básicas II

Adrián López Pascual

En este reto trabajamos con las bases de datos `empresa` y `videoclub` , que nos vienen dadas en los ficheros `empresa.sql` y `videoclub.sql`. A continuación realizamos una serie de consultas para extraer la información pedida en cada uno de los enunciados.

* [Enlace al repo](https://gitlab.com/Byadrian31/bases-de-datos)
* [Enlace al ejercicio](https://gitlab.com/Byadrian31/bases-de-datos/-/tree/main/UD_C1/reto2_empresa_videoclub?ref_type=heads)


## Query 1
Para seleccionar el código y descripción de todos los productos, seleccionaremos los atributos que se corresponden con las columnas `PROD_NUM` y `DESCRIPCIO`(realizando `AS` para una mejora estética) de la tabla `producte`, aunque como la tabla tiene únicamente dos columnas podemos llamarlas con `*`. Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
-- Muestre los productos (código y descripción) que comercializa la empresa.
USE empresa;
-- SELECT * FROM producte;
SELECT 
PROD_NUM AS "Código",
DESCRIPCIO AS "Descripción"
FROM producte;
```


## Query 2
Modificamos la consulta anterior añadiendo el filtro `LIKE` sobre la columna `DESCRIPCIO` para buscar únicamente los atributos que tengan la palabra `TENNIS`:

```sql
-- Muestre los productos (código y descripción) que contienen la palabra tenis en la descripción.
USE empresa;
SELECT 
PROD_NUM AS "Código",
DESCRIPCIO AS "Descripción"
FROM producte
WHERE DESCRIPCIO LIKE "%TENNIS%";
```


## Query 3
En este código selecciono los atributos: `CLIENT_COD`, `NOM`, `AREA` y `TELEFON`(realizando `AS` para una mejora estética) sobre la tabla `client`. 

```sql
-- Muestre el código, nombre, área y teléfono de los clientes de la empresa.
USE empresa;
SELECT 
CLIENT_COD AS "Código cliente",
NOM AS "Nombre",
AREA AS "Área",
TELEFON AS "Teléfono"
FROM client;
```

## Query 4
Modificando el código anterior, filtramos por todos los clientes que no tengan como area "636" (`!= 636`): 

```sql
-- Muestre los clientes (código, nombre, ciudad) que no son del área telefónica 636.
USE empresa;
SELECT
CLIENT_COD AS "Código cliente",
NOM AS "Nombre",
CIUTAT AS "Ciudad"
FROM client 
WHERE AREA != 636;
```

## Query 5
En esta consulta selecciono los atributos: `COM_NUM`,`COM_DATA`, y `DATA_TRAMESA`(realizando `AS` para una mejora estética) sobre la tabla `comanda`.

```sql
-- Muestre las órdenes de compra de la tabla de pedidos (código, fechas de orden y de envío)
USE empresa;
SELECT
COM_NUM AS "Código",
COM_DATA AS "Fecha de orden",
DATA_TRAMESA AS "Fecha de envío"
FROM comanda;
```

## Query 6
En esta consulta selecciono los atributos: `Nom` y `Telefon`(realizando `AS` para una mejora estética) sobre la tabla `client`.

```sql
-- Lista de nombres y teléfonos de los clientes.
USE videoclub;
SELECT 
Nom AS "Nombre",
Telefon AS "Teléfono"
FROM client;
```

## Query 7
En esta consulta selecciono los atributos: `Data` y `Import`(realizando `AS` para una mejora estética) sobre la tabla `factura`.

```sql
-- Lista de fechas e importes de las facturas.
USE videoclub;
SELECT 
Data AS "Fecha",
Import AS "Importe"
FROM factura;
```

## Query 8
En esta consulta selecciono el atributo: `Descripcio` (realizando `AS` para una mejora estética) sobre la tabla `detallfactura`, realizando un filtro sobre la `LiniaFactura 3`.

```sql
-- Lista de productos (descripción) facturados en la factura número 3.
USE videoclub;
SELECT
Descripcio AS "Descripción"
FROM  detallfactura
WHERE LiniaFactura = 3;
```

## Query 9
En esta consulta selecciono todos los atributos con `*` sobre la tabla `factura`, ordenándolos por importe de manera descendente.

```sql
-- Lista de facturas ordenada de forma decreciente por importe
USE videoclub;
SELECT
*
FROM factura
ORDER BY Import DESC;
```

## Query 10
En esta consulta selecciono todos los atributos con `*` sobre la tabla `actor`, filtrando con `LIKE` por todos los nombres que empiezen por X.

```sql
-- Lista de los actores cuyo nombre comience por X
USE videoclub;
SELECT
*
FROM actor
WHERE Nom LIKE "X%";
```