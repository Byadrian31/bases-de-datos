-- Muestre las órdenes de compra de la tabla de pedidos (código, fechas de orden y de envío)
USE empresa;
SELECT
COM_NUM AS "Código",
COM_DATA AS "Fecha de orden",
DATA_TRAMESA AS "Fecha de envío"
FROM comanda;