-- Lista de nombres y teléfonos de los clientes.
USE videoclub;
SELECT 
Nom AS "Nombre",
Telefon AS "Teléfono"
FROM client;