-- Muestre los clientes (código, nombre, ciudad) que no son del área telefónica 636.
USE empresa;
SELECT
CLIENT_COD AS "Código cliente",
NOM AS "Nombre",
CIUTAT AS "Ciudad"
FROM client 
WHERE AREA != 636;