-- Administradores que han dado más de  2 “Me gusta”.
USE instagram_low_cost;
SELECT u.idUsuario, u.nombre AS nombreUsuario, COUNT(rc.idTipoReaccion) AS totalMeGusta FROM usuarios u
JOIN roles r 
JOIN reaccionesComentarios rc 
JOIN tiposReaccion tr 
ON u.idRol = r.idRol
AND u.idUsuario = rc.idUsuario
AND rc.idTipoReaccion = tr.idTipoReaccion
WHERE r.descripcion = 'Administrador' 
AND tr.descripcion = 'Me gusta'
GROUP BY u.idUsuario HAVING totalMeGusta > 2;
