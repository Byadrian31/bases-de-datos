-- Número de “Me divierte” de la foto número 12 del usuario 45
USE instagram_low_cost;
SELECT COUNT(*) AS totalMeDivierte
FROM reaccionesFotos rf
JOIN tiposReaccion tr 
ON rf.idTipoReaccion = tr.idTipoReaccion
WHERE rf.idUsuario = 45
AND rf.idFoto = 12
AND tr.descripcion = 'Me divierte';
