# Reto 0: Consultas antiguas

Adrián López Pascual

En este reto trabajamos con la base de datos `instagram_low_cost` , que nos viene dada en el fichero `instagram_low_cost2.sql`. A continuación realizamos una serie de consultas para extraer la información pedida en cada uno de los enunciados.

* [Enlace al repo](https://gitlab.com/Byadrian31/bases-de-datos)
* [Enlace al ejercicio](https://gitlab.com/Byadrian31/bases-de-datos/-/tree/main/UD_C0/reto0_consultas_antiguas?ref_type=heads)


## Query 1
En esta consulta he decidido sacar el `idFoto` y `descripcion` de la tabla `fotos` filtrando por el usuario con id 36:

```sql
-- Fotos del usuario con ID 36
USE instagram_low_cost;
SELECT idFoto, descripcion FROM fotos
	WHERE idUsuario = 36;
```

## Query 2
En esta consulta he decidido sacar el `idFoto` y `descripcion` de la tabla `fotos`, filtrando por el usuario 36 y filtrando por fecha usando las funcions `Year()` y `Month()`:

```sql
-- Fotos del usuario con ID 36 tomadas en enero del 2023
USE instagram_low_cost;
SELECT idFoto, descripcion FROM fotos
	WHERE idUsuario = 36
    AND YEAR(fechaCreacion) = 2023 
    AND MONTH(fechaCreacion) = 1;
```


## Query 3
En esta consulta selecciono `idcomentario` y `comentario` de la tabla `comentarios`, realizo diferente `JOIN` con sus respectivos `AS` y los junto con las siguientes secuencias sql: `ON c.idComentario = cf.idComentario` y `AND cf.idFoto = f.idFoto`, y luego indico `c.idUsuario = 36`, `cf.idFoto = 12` y `f.idUsuario = 11`:

```sql
-- Comentarios del usuario 36 sobre la foto 12 del usuario 11
USE instagram_low_cost;
SELECT c.idComentario, c.comentario FROM comentarios as c
JOIN comentariosFotos as cf
JOIN fotos as f
ON c.idComentario = cf.idComentario
AND cf.idFoto = f.idFoto
WHERE c.idUsuario = 36
AND cf.idFoto = 12
AND f.idUsuario = 11;
```


## Query 4
En esta consulta selecciono `rf.idFoto` y `f.descripcion` de la tabla `fotos`(con su respectivo `AS`), realizo un `JOIN`(realizando `AS` para una mejora estética) de la tabla `reaccionesFotos`, las juntamos con la sentencia `ON f.idFoto = rf.idFoto` y finalmente filtramos la consulta con las sentencias : `WHERE rf.idUsuario = 25` y `AND rf.idTipoReaccion = 4`:

```sql
-- Fotos que han sorprendido al usuario 25
USE instagram_low_cost;
SELECT rf.idFoto , f.descripcion FROM fotos AS f
JOIN reaccionesFotos as rf
ON f.idFoto = rf.idFoto
WHERE rf.idUsuario = 25
AND rf.idTipoReaccion = 4;
```

## Query 5
En esta consulta decido sacar el `u.idUsuario`, `u.nombre`(referenciandolo(`AS`) como `nombreUsuario`) y realizar un `COUNT` de `rc.idTipoReaccion`(referenciandolo(`AS`) como `totalMeGusta`) de la tabla `usuarios(AS u)`, posteriormente realizo diferentes `JOIN` (que yo creo convenientes) con sus respectivos `AS` y las combino con las sentencias `ON` (`u.idRol = r.idRol`, `u.idUsuario = rc.idUsuario` y `rc.idTipoReaccion = tr.idTipoReaccion`) para asegurarme de sacar únicamente los usuarios con el rol de administrador(`r.descripcion = 'Administrador' `)  y que hayan dado Me gusta (`tr.descripcion = 'Me gusta'`).Finalmente con el Group By agrupo los resultados por el idUsuario para aplicar una condicion(HAVING) para que filtre por los usuarios que hayan dado más de dos "Me gusta" (`GROUP BY u.idUsuario HAVING totalMeGusta > 2`):

```sql
-- Administradores que han dado más de  2 “Me gusta”.
USE instagram_low_cost;
SELECT u.idUsuario, u.nombre AS nombreUsuario, COUNT(rc.idTipoReaccion) AS totalMeGusta FROM usuarios u
JOIN roles r 
JOIN reaccionesComentarios rc 
JOIN tiposReaccion tr 
ON u.idRol = r.idRol
AND u.idUsuario = rc.idUsuario
AND rc.idTipoReaccion = tr.idTipoReaccion
WHERE r.descripcion = 'Administrador' 
AND tr.descripcion = 'Me gusta'
GROUP BY u.idUsuario HAVING totalMeGusta > 2;
     

```

## Query 6
En esta consulta realizo un `COUNT` de todos los valores(referenciado(`AS`) con `totalMeDivierte`) de la tabla `reaccionesFotos(AS rf) `, uso un `JOIN` sobre la tabla `tiposReaccion(AS tr)` para luego combinarlas con la sentencia `ON rf.idTipoReaccion = tr.idTipoReaccion`, filtro por el usuario 45 (`rf.idUsuario = 45`) , el idFoto 12 (`rf.idFoto = 12`) y la descripcion "Me divierte" `tr.descripcion = 'Me divierte'` :

```sql
-- Número de “Me divierte” de la foto número 12 del usuario 45
USE instagram_low_cost;
SELECT COUNT(*) AS totalMeDivierte
FROM reaccionesFotos rf
JOIN tiposReaccion tr 
ON rf.idTipoReaccion = tr.idTipoReaccion
WHERE rf.idUsuario = 45
AND rf.idFoto = 12
AND tr.descripcion = 'Me divierte';

```

## Query 7
En esta última consulta realizo un `COUNT` de todos los valores(referenciado(`AS`) `totalFotosEnLaPlaya`) de la tabla `fotos`, filtrando por la `descripcion` usando `LIKE '%playa%'` para sacar todas las descripciones que contengan la palabra playa :

```sql
-- Número de fotos tomadas en la playa (en base al título)
USE instagram_low_cost;
SELECT COUNT(*) AS totalFotosEnLaPlaya
FROM fotos
WHERE descripcion LIKE '%playa%';
```
