-- Fotos que han sorprendido al usuario 25
USE instagram_low_cost;
SELECT rf.idFoto , f.descripcion FROM fotos AS f
JOIN reaccionesFotos as rf
ON f.idFoto = rf.idFoto
WHERE rf.idUsuario = 25
AND rf.idTipoReaccion = 4;