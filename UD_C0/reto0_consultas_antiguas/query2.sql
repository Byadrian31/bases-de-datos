-- Fotos del usuario con ID 36 tomadas en enero del 2023
USE instagram_low_cost;
SELECT idFoto, descripcion, fechaCreacion FROM fotos
	WHERE idUsuario = 36
    AND YEAR(fechaCreacion) = 2023 
    AND MONTH(fechaCreacion) = 1;