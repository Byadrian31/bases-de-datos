-- Número de fotos tomadas en la playa (en base al título)
USE instagram_low_cost;
SELECT COUNT(*) AS totalFotosEnLaPlaya
FROM fotos
WHERE descripcion LIKE '%playa%';