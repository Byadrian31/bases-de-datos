-- Comentarios del usuario 36 sobre la foto 12 del usuario 11
USE instagram_low_cost;
SELECT c.idComentario, c.comentario FROM comentarios as c
JOIN comentariosFotos as cf
JOIN fotos as f
ON c.idComentario = cf.idComentario
AND cf.idFoto = f.idFoto
WHERE c.idUsuario = 36
AND cf.idFoto = 12
AND f.idUsuario = 11;