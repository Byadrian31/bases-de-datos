# Unidad C0: Recapitulación

Autor Adrián López Pascual

En este documento explico de manera breve y concisa los diferentes apartados relacionados con las bases de datos así como: ¿que són las bases de datos?, ¿que son los DBMS?, ejemplos de estos y los más usados, modelo cliente-servidor, lenguaje SQL y por últimolas bd relacionales.

## Concepto y origen de las bases de datos

Las bases de datos son una herramienta encargadas de almacenar datos de manera organizada y estructurada.

Las bases de datos resuelven problemas como el almacenamiento, la organización, la recuperación y el análisis de datos.


## Sistemas de gestión de bases de datos

Un sistema de gestión de bases de datos es un software encargado de la gestión de la base de datos pudiendo realizar acciones como: edición, creación, eliminaciṕn y consultar datos.

Algunas de sus características son: una vista centralizada y clara de los datos, gestión adecuada de los datos, disponibilidad de SQL, integridad y seguridad, API's visuales e intuitivas y eficiencia.

### Ejemplos de sistemas de gestión de bases de datos

Actualmente los DBMS más usados son: MySQL, PostgreSQL, Microsoft SQL Server, Oracle Database, SQLite y MariaDB.

* Oracle DB "cliente-servidor".
* IMB Db2 "cliente-servidor".
* SQLite "software libre".
* MariaDB "software libre, cliente-servidor".
* SQL Server "cliente-servidor".
* PostgreSQL "software libre, cliente-servidor".
* mySQL "software libre, cliente-servidor".

## Modelo cliente-servidor

Es interesesante que el DBMS esté en el servidor porque se permite centralizar todos los datos en un único lugar accesible, además será mas fácil implementar medidas de seguridad en un solo lugar y además permite una mejor manejo de cargas de trabajo y almacenamiento de datos.

El modelo cliente-servidor se basa en la comunicación entre un cliente, que solicita y consume datos o servicios, y un servidor, que almacena y gestiona esos datos o servicios. Este modelo permite una arquitectura distribuida donde múltiples clientes pueden acceder a los recursos del servidor de manera simultánea.

* __Cliente__: Es una entidad que solicita servicios o recursos.
* __Servidor__: Es una entidad que proporciona servicios o recursos solicitados por los clientes.
* __Red__: conjunto de dispositivos electrónicos conectados entre sí que pueden intercambiar datos y recursos.
* __Puerto de escucha__: número de identificación asociado a un proceso o servicio en un sistema informático que está esperando recibir conexiones entrantes.
* __Petición__:  mensaje enviado por un cliente a un servidor solicitando un recurso o servicio específico.
* __Respuesta__: mensaje enviado por un servidor en respuesta a una petición recibida de un cliente.

## SQL

SQL(Structured Query Language) es un lenguaje de programación diseñado para administrar y manipular bases de datos relacionales.

SQL es un lenguaje declarativo, lo que significa que los usuarios especifican qué operaciones desean realizar en los datos, pero no cómo se deben llevar a cabo esas operaciones.

### Instrucciones de SQL
#### DDL
Lenguaje de Definición de Datos, se utiliza para definir y modificar la estructura de la base de datos y los objetos relacionados.
(CREATE, ALTER, DROP, TRUNCATE, RENAME)
#### DML
Lenguaje de Manipulación de Datos, se utiliza para realizar operaciones de manipulación y gestión de datos en la base de datos.
(SELECT, INSERT, UPDATE, DELETE, MERGE)
#### DCL
Lenguaje de Control de Datos, se utiliza para controlar los privilegios y permisos de acceso a los datos en la base de datos. 
(GRANT, REVOKE)
#### TCL
Lenguaje de Control de Transacciones, se utiliza para gestionar transacciones en la base de datos. 
(COMMIT, ROLLBACK, SAVEPOINT, SET TRANSACTION)

## Bases de datos relacionales

Una base de datos relacional es un tipo de base de datos que organiza los datos en forma de tablas relacionadas entre sí mediante llaves primarias y llaves foráneas. Teniendo ventajas como: estructura organizada, integridad de los datos, flexibilidad, escalabilidad y seguridad.

Los principales elementos que conforman las bases de datos relacionales son: tabla, columna, fila, llave primaria y llave foránea.

* __Relación (tabla)__:  una relación se refiere a la asociación entre dos o más entidades.
* __Atributo/Campo (columna)__:  representa la información que se almacena para cada instancia de la entidad.
* __Registro/Tupla (fila)__: es una instancia individual de datos almacenada en una tabla de una base de datos relacional.